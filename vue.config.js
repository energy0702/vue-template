module.exports = {
  //outputDir : '../chengkong.API/wwwroot',
  chainWebpack: config => {
    config
      .plugin('html')
      .tap(args => {
        args[0].title= ''
        return args
      })
  },
  configureWebpack: {
    module:{
        rules:[{
            test:/\.mjs$/,
            include: /node_modules/,
            type: "javascript/auto"
        }]
    },
  }
}

