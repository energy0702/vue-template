import { createApp } from 'vue'

// Element-plus
import ElementPlus from 'element-plus'
import lang from 'element-plus/lib/locale/lang/zh-tw'
import 'element-plus/dist/index.css'
import * as Icons from '@element-plus/icons-vue' // 引入所有Icons

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-icons-vue'

import App from './App.vue'

const app = createApp(App)
app.use(ElementPlus, { lang });

for (const name in Icons) {
    app.component(name, (Icons as any)[name])
  }


app.mount('#app')